Flex / Bison Parser
Language: C
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
         Gloire Rubambiza (https://github.com/rubambig)
         Frank Wanye (https://github.com/ffrankies)
Class: CIS 343 - Structure of Programming Languages
Semester / Year: Winter 2017

This program reverses a given file. For full specifications see 'flex bison desc.pdf'.