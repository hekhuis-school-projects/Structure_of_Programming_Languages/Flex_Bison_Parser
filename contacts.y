/* Checks for a valid contact given a contact or list of contacts
@author Gloire Rubambiza, Kyle Hekhuis, and Frank Wanye */

%code top{
#include <stdio.h>
int yylex();
int yyerror(const char *s);
}

/* Can hold either int or a string of chars */
%union{
  int iValue;
  char *sValue;
}

%start contactList
%token NAME
%type<sValue> NAME
%token PHONE
%type<sValue> PHONE
%token EMAIL
%type<sValue> EMAIL
%token STREET_NAME
%type<sValue> STREET_NAME
%token STREET_TYPE
%type<sValue> STREET_TYPE
%token STREET_NUM
%type<iValue> STREET_NUM
%token ZIPCODE
%type<iValue> ZIPCODE
%type<sValue> contact

%%

/* Rules */
contactList:	contact
|         		contact contactList
;

contact: 		NAME NAME STREET_NUM STREET_NAME STREET_TYPE ZIPCODE {printf("Valid contact!\n");}
;

%%


int main(int argc, char **argv){
  yyparse();
}

int yyerror(const char *msg) {
  return fprintf(stderr, "YACC: %s\n", msg );
}
