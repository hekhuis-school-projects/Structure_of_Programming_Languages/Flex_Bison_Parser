/* Returns tokens for phone numbers, names,
 emails, street names, street types, and zipcodes
@author Gloire Rubambiza, Kyle Hekhuis, and Frank Wanye */

%{

#include <stdio.h>
#include "contacts.tab.h" /* Links to contacts.y */

%}

%%

[A-Z][a-z]{3,} {return NAME; /* Must be at least 3 letters to be a name and start with capital */}

\([1-9]{3}\)[0-9]{3}-[0-9]{4} {return PHONE; /* Follows format (###)###-#### */}

[(A-z|0-9)]+@[(A-z|0-9)]+\.[(A-z|0-9)]+\.(com|net|org|edu|gov)	{return EMAIL; /* word@word.word.com */}

[0-9]{4} {return STREET_NUM; /* 4 digits */}

~[A-Z][a-z]+ {return STREET_NAME; /* Uses '~' to determine between name and street name. Must start with capital */}

(Dr|Rd|Av) {return STREET_TYPE; /* 'Dr' 'Rd' or 'Av' */}

[1-9][0-9]{4} {return ZIPCODE; /* 5 digits that doesn't start with 0 */}

%%
